bname=$(shell basename $(shell pwd))
name=/tmp/$(bname)$(shell date +%m%d).tar.gz
BACKUPDIR=~/software
NGINXDIR=/usr/share/nginx/html/app/

build:
	npm run build

backup:
	cd ..; tar --exclude='node_modules' --exclude='dist' -czf $(name) $(bname) && cp -iv $(name) $(BACKUPDIR)

install: build
	sudo cp -r ./dist/* $(NGINXDIR)

help:
	@echo "BACKUPDIR = ~/software"
	@echo "NGINXDIR  = /usr/share/nginx/html/app/"
	@echo ""
	@echo "make target:"
	@echo "    backup : archive code files to BACKUPDIR"
	@echo "    install: build project and copy dist/ to NGINXDIR"
	@echo "    build  : npm run build"
