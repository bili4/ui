import { myHeaders, settingStore } from "./counter";
import type {
  AllSearchParam,
  BangumiSearchParm,
  SpaceSearchParm,
  UserSearchParm,
} from "./interface";

enum ApiUrl {
  NewsUrl = "https://api.vc.bilibili.com/dynamic_svr/v1/dynamic_svr/dynamic_new?type_list=8,512,4097,4098,4099,4100,4101&uid=",
  UserInfoUrl = "https://api.bilibili.com/x/space/acc/info?jsonp=jsonp&mid=",
  SuggestionUrl = "https://s.search.bilibili.com/main/suggest?func=suggest&suggest_type=accurate&sub_type=tag&main_ver=v1&highlight=&bangumi_acc_num=1&special_acc_num=1&topic_acc_num=1&upuser_acc_num=3&tag_num=10&special_num=10&bangumi_num=10&upuser_num=3&term=",
  VideoInfoUrl = "https://api.bilibili.com/x/web-interface/view?bvid=",
  CookieUrl = "https://data.bilibili.com/v/web/web_page_view?url=https://www.bilibili.com/blackboard/friends-links.html",
  MoreNewsUrl = "https://api.vc.bilibili.com/dynamic_svr/v1/dynamic_svr/dynamic_history?type_list=8,512,4097,4098,4099,4100,4101",
  UserUrl = "https://api.bilibili.com/x/web-interface/search/type?context=&search_type=bili_user",
  BangumiUrl = "https://api.bilibili.com/x/web-interface/wbi/search/type?context=&order=",
}

class Api {
  /**创建网络请求 */
  async mkfetch(path: string, headers = myHeaders) {
    return fetch(settingStore.apiprefix + path, {
      headers,
    }).then((e) => e.json());
  }

  /**获取动态数据 */
  getNews(uid = settingStore.uid) {
    return this.mkfetch(ApiUrl.NewsUrl + uid);
  }
  /**获取更多动态数据 */
  getMoreNews(uid = settingStore.uid, offset_dynamic_id: number) {
    return this.mkfetch(
      ApiUrl.MoreNewsUrl + `&uid=${uid}&offset_dynamic_id=${offset_dynamic_id}`
    );
  }
  /**普通搜索请求 */
  searchVideo(param: AllSearchParam) {
    const { page, order, keyword, duration } = param;
    const searchurl = `https://api.bilibili.com/x/web-interface/search/type?page=${page}&order=${order}&keyword=${keyword}&duration=${duration}&search_type=video`;

    return this.mkfetch(searchurl);
  }

  /**用户空间搜索请求 */
  searchSpace(param: SpaceSearchParm) {
    const { mid, page, keyword, order } = param;
    const searchurl = `https://api.bilibili.com/x/space/arc/search?mid=${mid}&ps=30&tid=0&pn=${page}&keyword=${keyword}&order=${order}&jsonp=jsonp`;

    return this.mkfetch(searchurl);
  }

  /**用户信息请求 */
  searchUserInfo(mid: number) {
    return this.mkfetch(ApiUrl.UserInfoUrl + mid);
  }

  /**番剧和影视搜索请求 */
  searchBgFt(param: BangumiSearchParm) {
    return this.mkfetch(
      ApiUrl.BangumiUrl +
        "&search_type=" +
        param.search_type +
        "&keyword=" +
        param.keyword +
        "&page=" +
        param.page
    );
  }

  /**搜索建议词请求 */
  getSuggestion(keyword: string, uid = settingStore.uid) {
    return this.mkfetch(ApiUrl.SuggestionUrl + keyword + "&userid=" + uid);
  }
  /**视频信息请求 */
  getVideoInfo(bvid: string) {
    return this.mkfetch(ApiUrl.VideoInfoUrl + bvid);
  }

  searchUpUser(param: UserSearchParm) {
    const { page, keyword, order, order_sort: order_sort_tmp } = param;
    let order_sort = order_sort_tmp;
    if (order === "0") {
      order_sort = 0;
    }

    return this.mkfetch(
      `${ApiUrl.UserUrl}&page=${page}&keyword=${keyword}&order=${order}&order_sort=${order_sort}`
    );
  }
  /**获取游客Cookie */
  async getVisitorCookie() {
    const e = await fetch(settingStore.apiprefix + ApiUrl.CookieUrl, {
      referrerPolicy: "no-referrer",
    });

    return { cookie: e.headers.get("x-cookie") };
  }
}

const apiClient = new Api();

export default apiClient;
