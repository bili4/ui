# biliui

这是一个简化版的B站前端页面项目。

已实现的主要功能：
- 个人动态
- 视频搜索
- 用户搜索
- 用户主页视频稿件
- 视频播放

更多介绍请查看本项目的[Wiki页面](https://gitlab.com/bili4/ui/-/wikis/home)。